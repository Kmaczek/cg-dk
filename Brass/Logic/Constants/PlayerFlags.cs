﻿using System;

namespace Brass.Logic.Constants
{
    //Use System.Enum to manipulate flags

    /// <summary>
    /// Negative states that Player can have
    /// </summary>
    [Flags]
    public enum PlayerState : int
    {
        None = 0,
        Burning = 1 << 0,
        Vulnerable = 1 << 1,
        Shocked = 1 << 2,
        CantAttack = 1 << 3,
        Silenced = 1 << 4,
        Regenerating = 1 << 14,
        ReducedManaCost = 1 << 15,
        IncreasedMight = 1 << 16
    }


}