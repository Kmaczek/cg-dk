﻿using System;

namespace Brass.Logic.Constants
{
    /// <summary>
    /// Makes easier to recognize type of Thing
    /// </summary>
    public enum ThingType : short
    {
        None = 0,
        Player = 1,
        Unit,
        Spell,
        Ability,
        Item
    }
}