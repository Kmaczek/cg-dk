﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Brass.Logic.Constants;
using Brass.Logic.General;
using Brass.Logic.Helpers;
using Brass.Logic.Players;
using Brass.Logic.Units;

namespace Brass.Logic.Spells
{
    public class HealthSpell : SpellBase, ICastable
    {
        private short _amount;

            #region Constructors

        public HealthSpell(Player owner, Player oponent, short id)
            : base(owner, oponent, id)
        {
            //States = SpellState.InterruptImmune;
            ParseData();
        }

            #endregion

            #region Methods

        //public bool IsProperTarget(ThingBase target)
        //{
        //    foreach (var t in Targets)
        //    {
        //        if (target.TargetType.HasFlag(t.Affiliation))
        //        {
        //            if(target)
        //        }
        //    }
        //    return false;
        //}

        public void Cast(List<ThingBase> targets)
        {
            if (Oponent == null || Owner == null || targets == null) return;

            foreach (var v in targets)
            {
                if (v.GetType() == typeof(Player))
                {
                    var player = (Player)v;
                    player.Attributes.Health += _amount;
                }
                else if (v.TargetType == ThingType.Unit)
                {
                    var unit = (UnitBase)v;
                    unit.Attributes.Health += _amount;
                    //Console.WriteLine("HP: {0}", unit.Attributes.Health);
                }
            }
        }

        public void Uncast()
        {
            throw new NotImplementedException();
        }

        protected override sealed void ParseData()
        {
            var stuff = XElement.Parse(Properties.Resources.SpellTesting);
            var result = GetSpellElement(stuff);
            ParseBasic(result);
            
            var helper = new XmlHelper();
            _amount = helper.ParseNumberShort(result.Element(XHealthSpell.Amount));
            //Console.WriteLine(Attributes.ToString());
        }

            #endregion
        

    }

    /// <summary>
    /// Class helpfull when parsing from xml
    /// </summary>
    public static class XHealthSpell
    {
        public static String Amount = "Amount";
    }
}
