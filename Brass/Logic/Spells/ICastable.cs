﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Brass.Logic.Constants;
using Brass.Logic.General;

namespace Brass.Logic.Spells
{
    public interface ICastable
    {
        void Cast(List<ThingBase> targets = null);
        //bool IsTargetProper(ThingBase target);
        void Uncast();
    }

}   
