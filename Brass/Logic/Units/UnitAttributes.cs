﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Brass.Logic.Units
{
    [DebuggerDisplay("Health={Health}, Might={Might}, Resistance={Resistance}, " +
                     "Quickness={Quickness}, GoldCost={GoldCost}, SellCost={SellCost}")]
    public class UnitAttributes
    {
        public short Health { get; set; }
        public short Might { get; set; }
        public short Resistance { get; set; }
        public short Magic { get; set; }
        public short Quickness { get; set; }

        public short GoldCost { get; set; }
        public short GoldCostCurrent { get; set; }
        public short SellCost { get; set; }
        public short SellCostCurrent { get; set; }

    }   


}
